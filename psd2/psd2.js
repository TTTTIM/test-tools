function buildCmd(){
    let inputUrl = document.getElementById('url').value;
    let url = inputUrl.replace(/\&/g, '\\&')
    let adb = 'adb shell am start -a "android.intent.action.VIEW" '
    
    return adb + '"' + url + '"'
};

// Copy the value inside the textfield
function copy(copyText) {
    copyText.select();
    document.execCommand("copy");
    
    fadeOutMessage();             
}

// Copied to clipboard tooltip fading
function fadeOutMessage() {
    let copied = document.getElementById("copiedMSG")
    copied.style.display = "block";
    copied.classList.add("elementToFadeInAndOut");

    setTimeout(function () {
        copied.style.display = "none";
        copied.classList.remove("elementToFadeInAndOut");
    }, 2000);
}

function generateCmd(){
    let urlCheck = document.getElementById("url").validity.valid
    
    if (urlCheck == false) {
        document.getElementById('url').style.borderColor = "red";
    }else{
        document.getElementById('url').style.borderColor = "";
    } 

    if (urlCheck == false) {
        let errorMSG = document.getElementById("errorMSG")
        errorMSG.style.display = "block";
        errorMSG.classList.add("elementToFadeInAndOut");

        setTimeout(function () {
            errorMSG.style.display = "none";
            errorMSG.classList.remove("elementToFadeInAndOut");
        }, 2000);

        return false
    }

    let cmd = buildCmd()
    console.log(cmd)
    
    var input = document.getElementById('generatedCmd')
    input.value = cmd

    copy(input)
};
